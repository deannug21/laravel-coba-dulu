<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class castcontroller extends Controller
{
public function create(){
    return view('cast.create');
    }
    public function store(Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $queryDB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio']
        ]);
        
        return redirect('/cast/create');
    }
}